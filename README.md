# Bitbucket Pipelines Pipe:  alexbaileyuk/npm-audit-insights

A pipe to send the output of an NPM or Yarn audit to Bitbucket Code Insights.

## YAML Definition
Add the following snippet to the script section of your bitbucket-pipelines.yml file:
```yaml
- pipe: docker://alexbaileyuk/npm-audit-insights:latest
  variables:
    # PACKAGE_MANAGER: '<string>' # Optional
    # PACKAGE_JSON_PATH: '<string>' # Optional
    # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable | Usage |
| --- | --- |
| PACKAGE_MANAGER | This can be `yarn` or `npm`. Default: `npm` |
| PACKAGE_JSON_PATH | Location of your package.json and package-lock.json from your repository root. Default: `.` |
| DEBUG | Turn on extra debug information. Default: `false`. |

## Screenshots

![Screenshot](screenshots/report-screenshot.png)

## Examples

```yaml
- pipe: docker://alexbaileyuk/npm-audit-insights:latest
  variables:
    PACKAGE_JSON_PATH: './src'
```

## Support

If you'd like help with this pipe, or you have an issue or feature request, let me know by raising an issue in Bitbucket.

If you're reporting an issue, please include:
- the version of the pipe
- relevant logs and error messages
- steps to reproduce
- relevant package and lock file contents