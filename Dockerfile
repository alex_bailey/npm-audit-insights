FROM node:14.15-alpine3.11

RUN apk add yarn

COPY ./pipe/ /pipe/
RUN chmod +x /pipe/audit.sh

# Build and remove source files
WORKDIR /pipe
RUN npm install && npm run build
RUN rm src/ -r

# Below is just testing things...
COPY ./test/ /test/

ENTRYPOINT ["sh", "/pipe/audit.sh"]
