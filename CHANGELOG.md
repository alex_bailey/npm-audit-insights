# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.0

- minor: Added option to generate audit via Yarn and yarn.lock
- minor: Fixed counts
- patch: Made report build number unique.
- patch: Patch to test pipelines. No real changes.
- patch: Semversioner fix.
- patch: Semversioner fix.
- patch: Updated some documentation parts within the repo.

## 1.0.0

- major: Release 1.0
