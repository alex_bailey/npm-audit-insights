import { Logger } from './logger';

export class Config {
    public static DEBUG: boolean = process.env.DEBUG === 'true';
    public static PACKAGE_MANAGER: string = process.env.PACKAGE_MANAGER ?? 'npm';
    public static AUDIT_JSON_FILE: string = process.env.AUDIT_JSON_FILE ?? '/pipe/audit_output.json';
    public static BITBUCKET_REPO_OWNER: string = process.env.BITBUCKET_REPO_OWNER ?? '';
    public static BITBUCKET_REPO_SLUG: string = process.env.BITBUCKET_REPO_SLUG ?? '';
    public static BITBUCKET_COMMIT: string = process.env.BITBUCKET_COMMIT ?? '';
    public static BITBUCKET_BUILD_NUMBER: string = process.env.BITBUCKET_BUILD_NUMBER ?? '';

    public static checkConfig(): void {
        const notEmptyString = [
            Config.AUDIT_JSON_FILE,
            Config.BITBUCKET_REPO_OWNER,
            Config.BITBUCKET_REPO_SLUG,
            Config.BITBUCKET_COMMIT,
            Config.PACKAGE_MANAGER,
            Config.BITBUCKET_BUILD_NUMBER
        ];

        const supportedPackageManagers = [
            'npm', 'yarn'
        ];

        if (notEmptyString.includes('')) {
            throw new Error('Invalid configuration. Configuration value is missing.');
        }

        if (!supportedPackageManagers.includes(Config.PACKAGE_MANAGER)) {
            throw new Error('Invalid package manager.');
        }
    }

    public static logConfig() {
        Logger.debug(`Debug Mode: ${Config.DEBUG}`);
        Logger.debug(`Audit JSON File: ${Config.AUDIT_JSON_FILE}`);
        Logger.debug(`BB Repo Owner: ${Config.BITBUCKET_REPO_OWNER}`);
        Logger.debug(`BB Repo Slug: ${Config.BITBUCKET_REPO_SLUG}`);
        Logger.debug(`BB Commit Hash: ${Config.BITBUCKET_COMMIT}`);
        Logger.debug(`BB Build Number: ${Config.BITBUCKET_BUILD_NUMBER}`);
        Logger.debug(`Package Manager: ${Config.PACKAGE_MANAGER}`);
    }
}