export class ArrayHelpers {
    public static ChunkArray = <T>(inputArr: T[], chunkSize: number) => {
        return inputArr.reduce((resultArray: any[], item: any, index: number) => {
            const chunkIndex = Math.floor(index / chunkSize);

            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = [];
            }

            resultArray[chunkIndex].push(item);

            return resultArray;
        }, []);
    }
}