import { Config } from './config';
import { Report } from './models';

/**
 * We're using IIFE here to be able to use "top level" await. When running from the shell node will wait for the execution
 * stack to finish so this should be a safe way to achieve a top level await.
 */
(async () => {
    Config.logConfig();
    Config.checkConfig();

    let report;

    // Create report
    switch(Config.PACKAGE_MANAGER) {
        case 'npm':
            report = Report.createFromNpmAuditFile(Config.AUDIT_JSON_FILE);
            break;
        case 'yarn':
            report = Report.createFromYarnAuditFile(Config.AUDIT_JSON_FILE);
            break;
        default:
            throw new Error('Unknown package manager.');
    }

    // Send report to BB reports
    await report.uploadReport();

    // Send annotations to BB annotations
    await report.uploadAnnotations();
})();
