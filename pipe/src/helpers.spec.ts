import { ArrayHelpers } from "./helpers";

describe('When chunking arrays with a chunk size of five', () => {
    it('It should chunk an array of three objects in to one chunk.', () => {
        const resultChunks = ArrayHelpers.ChunkArray<number>([1, 2, 3], 5);
        expect(resultChunks).toHaveLength(1);
        expect(resultChunks[0]).toEqual([1, 2, 3]);
    })

    it('It should chunk an array of six objects in to two chunks.', () => {
        const resultChunks = ArrayHelpers.ChunkArray<number>([1, 2, 3, 4, 5, 6], 5);
        expect(resultChunks).toHaveLength(2);
        expect(resultChunks[0]).toEqual([1, 2, 3, 4, 5]);
        expect(resultChunks[1]).toEqual([6]);
    })

    it('It should chunk an array of eleven objects in to three chunks.', () => {
        const resultChunks = ArrayHelpers.ChunkArray<number>([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 5);
        expect(resultChunks).toHaveLength(3);
        expect(resultChunks[0]).toEqual([1, 2, 3, 4, 5]);
        expect(resultChunks[1]).toEqual([6, 7, 8, 9, 10]);
        expect(resultChunks[2]).toEqual([11]);
    })
})