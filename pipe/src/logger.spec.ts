jest.mock('./config', () => ({
    __esModule: true, // this property makes it work
    default: 'mockedDefaultExport',
    Config: { DEBUG: true }
}));

import { Logger } from './logger';
import { Config } from './config';

let consoleSpy = jest.spyOn(global.console, 'log');

describe('When debug mode is disabled', () => {
    beforeAll(() => {
        Config.DEBUG = false
    })

    afterEach(() => {
        consoleSpy.mockClear();
    })

    it('It should not log debug messages.', () => {
        let consoleSpy = jest.spyOn(global.console, 'log');

        Logger.debug('test message');
        expect(consoleSpy).toHaveBeenCalledTimes(0);

        consoleSpy.mockClear();
    })

    it('It should log info messages.', () => {
        let consoleSpy = jest.spyOn(global.console, 'log');

        Logger.info('test message');
        expect(consoleSpy).toHaveBeenCalledTimes(1);

        consoleSpy.mockClear();
    })

    it('It should log error messages.', () => {
        let consoleSpy = jest.spyOn(global.console, 'log');

        Logger.error('test message');
        expect(consoleSpy).toHaveBeenCalledTimes(1);

        consoleSpy.mockClear();
    })
})

describe('When debug mode is enabled', () => {
    beforeAll(() => {
        Config.DEBUG = true
    })

    afterEach(() => {
        consoleSpy.mockClear();
    })

    it('It should log debug messages.', () => {
        Logger.debug('test message');
        expect(consoleSpy).toHaveBeenCalledTimes(1);
    })

    it('It should log info messages.', () => {
        Logger.info('test message');
        expect(consoleSpy).toHaveBeenCalledTimes(1);
    })

    it('It should log error messages.', () => {
        Logger.error('test message');
        expect(consoleSpy).toHaveBeenCalledTimes(1);
    })
})