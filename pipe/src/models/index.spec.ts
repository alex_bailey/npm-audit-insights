import * as moduleExports from './index';

describe('When exporting objects', () => {
    it('It should should only export the Report class.', () => {
        expect(Object.keys(moduleExports)).toHaveLength(1);
        expect(moduleExports.Report).not.toBeUndefined();
    })
})