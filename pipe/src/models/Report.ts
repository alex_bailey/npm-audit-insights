import fs from 'fs';

import { AxiosResponse } from 'axios';
import { Logger } from '../logger';
import { UploadReport, UploadAnnotations } from '../api';
import { ArrayHelpers } from '../helpers';
import {debuglog} from "util";
import {Config} from "../config";

const AUDIT_TO_BB_MAP = new Map<string, string>([
    ['LOW', 'LOW'],
    ['MODERATE', 'MEDIUM'],
    ['HIGH', 'HIGH'],
    ['CRITICAL', 'CRITICAL']
]);

export interface BitbucketReport {
    title: string;
    details: string;
    report_type: 'SECURITY' | 'COVERAGE' | 'TEST' | 'BUG';
    reporter: string;
    result: 'FAILED' | 'PASSED' | 'PENDING';
    data: BitbucketReportDataItem[];
}

export interface BitbucketReportDataItem {
    title: string,
    type: 'NUMBER' | 'BOOLEAN' | 'DATE' | 'DURATION' | 'LINK' | 'PERCENTAGE' | 'TEXT',
    value: any
}

export interface BitbucketAnnotation {
    external_id: string;
    annotation_type: string;
    summary: string;
    details: string;
    severity: string;
    link: string;
}

export class Report {
    public lowVulnerabilities: number = 0;
    public moderateVulnerabilities: number = 0;
    public highVulnerabilities: number = 0;
    public criticalVulnerabilities: number = 0;
    public totalDependencies: number = 0;
    public totalVulnerabilities: number = 0;
    private annotations: BitbucketAnnotation[] = [];

    private constructor() { }

    public static createFromNpmAuditFile = (file: string): Report => {
        Logger.debug('Attempting to create report from NPM audit JSON.');

        const jsonAudit = JSON.parse(fs.readFileSync(file).toString());
        const auditMeta = jsonAudit.metadata;

        const report = new Report();

        report.lowVulnerabilities = auditMeta.vulnerabilities.low;
        report.moderateVulnerabilities = auditMeta.vulnerabilities.moderate;
        report.highVulnerabilities = auditMeta.vulnerabilities.high;
        report.criticalVulnerabilities = auditMeta.vulnerabilities.critical;

        report.totalDependencies = auditMeta.totalDependencies;
        report.totalVulnerabilities = report.lowVulnerabilities + report.moderateVulnerabilities +
            report.highVulnerabilities + report.criticalVulnerabilities;

        Object.keys(jsonAudit.advisories).forEach(advisoryKey => {
            const advisory = jsonAudit.advisories[advisoryKey];

            const findings = advisory.findings.reduce((subtotal: number, finding: any) => subtotal + finding.paths.length, 0);
            const multiplierStr = findings > 1 ? `(x${findings} occurrences)` : '';

            report.annotations.push({
                external_id: advisoryKey.toString(),
                annotation_type: 'VULNERABILITY',
                summary: `${advisory.cwe} - ${advisory.module_name} - ${advisory.title} ${multiplierStr}`,
                details: advisory.overview,
                severity: AUDIT_TO_BB_MAP.get(advisory.severity.toUpperCase()) || 'CRITICAL',
                link: advisory.url
            });
        });

        return report;
    }

    public static createFromYarnAuditFile = (file: string): Report => {
        Logger.debug('Attempting to create report from Yarn audit JSON.');

        const jsonAudit = JSON.parse(fs.readFileSync(file).toString());
        const report = new Report();

        const addedAnnotations: string[] = [];

        jsonAudit.forEach((auditObject: any) => {
            switch (auditObject.type) {
                case 'auditSummary':
                    Logger.debug('Adding audit summary details.');
                    report.lowVulnerabilities = auditObject.data.vulnerabilities.low;
                    report.moderateVulnerabilities = auditObject.data.vulnerabilities.moderate;
                    report.highVulnerabilities = auditObject.data.vulnerabilities.high;
                    report.criticalVulnerabilities = auditObject.data.vulnerabilities.critical;

                    report.totalDependencies = auditObject.data.totalDependencies;
                    report.totalVulnerabilities = report.lowVulnerabilities + report.moderateVulnerabilities +
                        report.highVulnerabilities + report.criticalVulnerabilities;
                    break;
                case 'auditAdvisory':
                    Logger.debug('Adding advisory notice data as annotation.');
                    const advisory = auditObject.data.advisory;
                    const annotationId = `${advisory.id}-${advisory.module_name}`;

                    if (!addedAnnotations.includes(annotationId)) {
                        report.annotations.push({
                            external_id: annotationId,
                            annotation_type: 'VULNERABILITY',
                            summary: `${advisory.cwe} - ${advisory.module_name} - ${advisory.title}`,
                            details: advisory.overview,
                            severity: AUDIT_TO_BB_MAP.get(advisory.severity.toUpperCase()) || 'CRITICAL',
                            link: advisory.url
                        });

                        addedAnnotations.push(annotationId);
                    }
                    break;
                default:
                    debuglog(`Ignoring unknown type '${auditObject.type}'`);
            }
        });

        return report;
    }

    public hasVulnerabilities = (): boolean => this.totalVulnerabilities > 0;

    public uploadReport = async (): Promise<AxiosResponse> => await UploadReport(this);

    public uploadAnnotations = async (): Promise<AxiosResponse[]> => await UploadAnnotations(this);

    public getAnnotations = (): BitbucketAnnotation[] => this.annotations;

    public addAnnotation = (annotation: BitbucketAnnotation): void => {
        this.annotations.push(annotation);
    }

    public toBitbucketReportFormat = (): BitbucketReport => {
        return {
            title: `[${Config.BITBUCKET_BUILD_NUMBER}] ${Config.PACKAGE_MANAGER} audit report`,
            details: `Package vulnerabilities reported by ${Config.PACKAGE_MANAGER} audit.`,
            report_type: 'SECURITY',
            reporter: 'npm-audit-insights',
            result: this.hasVulnerabilities() ? 'FAILED' : 'PASSED',
            data: [
                {
                    title: 'Low Severity',
                    type: 'NUMBER',
                    value: this.lowVulnerabilities
                },
                {
                    title: 'Moderate Severity',
                    type: 'NUMBER',
                    value: this.moderateVulnerabilities
                },
                {
                    title: 'High Severity',
                    type: 'NUMBER',
                    value: this.highVulnerabilities
                },
                {
                    title: 'Critical Severity',
                    type: 'NUMBER',
                    value: this.criticalVulnerabilities
                },
                {
                    title: 'Total Dependencies',
                    type: 'NUMBER',
                    value: this.totalDependencies
                },
                {
                    title: 'Total Vulnerabilities',
                    type: 'NUMBER',
                    value: this.totalVulnerabilities
                }
            ]
        }
    }

    public getAnnotationsInChunks = (chunkSize: number): BitbucketAnnotation[] =>
        ArrayHelpers.ChunkArray<BitbucketAnnotation>(this.annotations, chunkSize);
}