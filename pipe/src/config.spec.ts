jest.mock('./logger', () => ({
    __esModule: true, // this property makes it work
    default: 'mockedDefaultExport',
    Logger: ({
        debug: jest.fn()
    })
}));

import { Config } from './config';
import { Logger } from './logger';

describe('When checking the configuration', () => {
    beforeEach(() => {
        Config.DEBUG = true;
        Config.AUDIT_JSON_FILE = 'FILE';
        Config.BITBUCKET_REPO_OWNER = 'REPO_OWNER';
        Config.BITBUCKET_REPO_SLUG = 'REPO_SLUG';
        Config.BITBUCKET_COMMIT = 'COMMIT_HASH';
        Config.PACKAGE_MANAGER = 'npm';
        Config.BITBUCKET_BUILD_NUMBER = '1';
    })

    it('It should throw an error if the AUDIT_JSON_FILE is an empty string.', () => {
        Config.AUDIT_JSON_FILE = '';

        expect(() => {
            Config.checkConfig();
        }).toThrow(new Error('Invalid configuration. Configuration value is missing.'));
    })

    it('It should throw an error if the BITBUCKET_REPO_OWNER is an empty string.', () => {
        Config.BITBUCKET_REPO_OWNER = '';

        expect(() => {
            Config.checkConfig();
        }).toThrow(new Error('Invalid configuration. Configuration value is missing.'));
    })

    it('It should throw an error if the BITBUCKET_REPO_SLUG is an empty string.', () => {
        Config.BITBUCKET_REPO_SLUG = '';

        expect(() => {
            Config.checkConfig();
        }).toThrow(new Error('Invalid configuration. Configuration value is missing.'));
    })

    it('It should throw an error if the BITBUCKET_COMMIT is an empty string.', () => {
        Config.BITBUCKET_COMMIT = '';

        expect(() => {
            Config.checkConfig();
        }).toThrow(new Error('Invalid configuration. Configuration value is missing.'));
    })

    it('It should throw an error if the PACKAGE_MANAGER is an empty string.', () => {
        Config.PACKAGE_MANAGER = '';

        expect(() => {
            Config.checkConfig();
        }).toThrow(new Error('Invalid configuration. Configuration value is missing.'));
    })

    it('It should throw an error if the BITBUCKET_BUILD_NUMBER is an empty string.', () => {
        Config.BITBUCKET_BUILD_NUMBER = '';

        expect(() => {
            Config.checkConfig();
        }).toThrow(new Error('Invalid configuration. Configuration value is missing.'));
    })

    it('It should throw an error if the PACKAGE_MANAGER is not yarn or npm.', () => {
        Config.PACKAGE_MANAGER = 'fdsafda';

        expect(() => {
            Config.checkConfig();
        }).toThrow(new Error('Invalid package manager.'));

        Config.PACKAGE_MANAGER = 'yarn';

        expect(() => {
            Config.checkConfig();
        }).not.toThrow(new Error('Invalid package manager.'));

        Config.PACKAGE_MANAGER = 'npm';

        expect(() => {
            Config.checkConfig();
        }).not.toThrow(new Error('Invalid package manager.'));
    })
})

describe('When logging configuration data', () => {
    beforeAll(() => {
        Config.DEBUG = true;
        Config.AUDIT_JSON_FILE = 'FILE';
        Config.BITBUCKET_REPO_OWNER = 'REPO_OWNER';
        Config.BITBUCKET_REPO_SLUG = 'REPO_SLUG';
        Config.BITBUCKET_COMMIT = 'COMMIT_HASH';
        Config.PACKAGE_MANAGER = 'npm';
        Config.BITBUCKET_BUILD_NUMBER = '1';
    })

    it('It should log the required configuration to debug.', () => {
        Config.logConfig();
        expect(Logger.debug).toHaveBeenCalledTimes(7);
        expect(Logger.debug).toHaveBeenCalledWith(`Debug Mode: true`);
        expect(Logger.debug).toHaveBeenCalledWith(`Audit JSON File: FILE`);
        expect(Logger.debug).toHaveBeenCalledWith(`BB Repo Owner: REPO_OWNER`);
        expect(Logger.debug).toHaveBeenCalledWith(`BB Repo Slug: REPO_SLUG`);
        expect(Logger.debug).toHaveBeenCalledWith(`BB Commit Hash: COMMIT_HASH`);
        expect(Logger.debug).toHaveBeenCalledWith(`BB Build Number: 1`);
        expect(Logger.debug).toHaveBeenCalledWith(`Package Manager: npm`);
    })
})