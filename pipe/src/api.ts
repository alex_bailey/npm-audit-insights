import axios from 'axios';
import { AxiosResponse } from 'axios';
import { Config } from './config';
import { Report }  from './models';
import { Logger }  from './logger';

const api = axios.create({
    baseURL: `http://api.bitbucket.org/2.0/repositories/${Config.BITBUCKET_REPO_OWNER}/${Config.BITBUCKET_REPO_SLUG}/commit/${Config.BITBUCKET_COMMIT}/reports/npm-audit-insights-${Config.BITBUCKET_BUILD_NUMBER}`,
    timeout: 5000,
    headers: { 'Content-Type': 'application/json' },
    proxy: { host: 'host.docker.internal', port: 29418 }
});

const UploadReport = async (report: Report): Promise<AxiosResponse> => {
    Logger.debug('Uploading report to code insights.');

    return await api.put('', report.toBitbucketReportFormat());
}

const UploadAnnotations = async (report: Report): Promise<AxiosResponse[]> => {
    Logger.debug('Uploading annotations to code insights.');

    const annotationChunks = report.getAnnotationsInChunks(100);
    return await Promise.all(annotationChunks.map(async (annotationChunk) => {
        Logger.debug(JSON.stringify(annotationChunk));
        return await api.post('/annotations', annotationChunk);
    }));
};

export { UploadReport, UploadAnnotations };