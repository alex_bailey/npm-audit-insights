import { Config } from './config';

export class Logger {
    public static debug = (msg: string) => {
        if (Config.DEBUG) console.log('[DEBUG] ' + msg);
    }

    public static info = (msg: string) => {
        console.log('[INFO] ' + msg);
    }

    public static error = (msg: string) => {
        console.log('[ERROR] ' + msg);
    }
}