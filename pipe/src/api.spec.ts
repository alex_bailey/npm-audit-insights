const putMock = jest.fn();
const postMock = jest.fn();

const axiosMock = {
    create: jest.fn(() => ({
        put: putMock,
        post: postMock
    }))
}

jest.mock('axios', () => ({
    __esModule: true, // this property makes it work
    default: axiosMock
}));

import axios from 'axios';
import { Report } from './models';
import * as moduleExports from './api';
import { UploadReport, UploadAnnotations } from './api';

describe('When exporting objects', () => {
    it('It should only export the UploadReport and UploadAnnotations functions.', () => {
        expect(Object.keys(moduleExports)).toHaveLength(2);
        expect(moduleExports.UploadReport).not.toBeUndefined();
        expect(moduleExports.UploadAnnotations).not.toBeUndefined();
    })
})

describe('When running an npm audit', () => {
    afterEach(() => {
        putMock.mockClear();
        postMock.mockClear();
    })

    describe('When uploading the report to code-insights', () => {
        it('It should send a PUT request to the correct endpoint.', async () => {
            const report = Report.createFromNpmAuditFile(`${__dirname}/../test-fixtures/npm_audit.no-vulnerabilities.json`);
            await UploadReport(report)

            expect(putMock).toHaveBeenCalledTimes(1);
            expect(putMock).toHaveBeenCalledWith('', report.toBitbucketReportFormat());
        })
    })

    describe('When uploading the annotations to code-insights when there are no vulnerabilities', () => {
        it('It should not send any network requests.', async () => {
            const report = Report.createFromNpmAuditFile(`${__dirname}/../test-fixtures/npm_audit.no-vulnerabilities.json`);
            await UploadAnnotations(report)

            expect(postMock).toHaveBeenCalledTimes(0);
        })
    })

    describe('When uploading the annotations to code-insights when there are under 100 vulnerabilities', () => {
        it('It should send one POST request to the correct endpoint.', async () => {
            const report = Report.createFromNpmAuditFile(`${__dirname}/../test-fixtures/npm_audit.vulnerabilities.json`);
            await UploadAnnotations(report)

            expect(postMock).toHaveBeenCalledTimes(1);
        })
    })

    describe('When uploading the annotations to code-insights when there are over 100 vulnerabilities', () => {
        it('It should send multiple POST request to the correct endpoint.', async () => {
            const report = Report.createFromNpmAuditFile(`${__dirname}/../test-fixtures/npm_audit.vulnerabilities.json`);

            while (report.getAnnotations().length <= 100) {
                report.addAnnotation({
                    annotation_type: "", details: "", external_id: "", link: "", severity: "", summary: ""
                });
            }

            await UploadAnnotations(report)

            expect(postMock).toHaveBeenCalledTimes(2);
        })
    })
})

describe('When running a yarn audit', () => {
    afterEach(() => {
        putMock.mockClear();
        postMock.mockClear();
    })

    describe('When uploading the report to code-insights', () => {
        it('It should send a PUT request to the correct endpoint.', async () => {
            const report = Report.createFromYarnAuditFile(`${__dirname}/../test-fixtures/yarn_audit.no-vulnerabilities.json`);
            await UploadReport(report)

            expect(putMock).toHaveBeenCalledTimes(1);
            expect(putMock).toHaveBeenCalledWith('', report.toBitbucketReportFormat());
        })
    })

    describe('When uploading the annotations to code-insights when there are no vulnerabilities', () => {
        it('It should not send any network requests.', async () => {
            const report = Report.createFromYarnAuditFile(`${__dirname}/../test-fixtures/yarn_audit.no-vulnerabilities.json`);
            await UploadAnnotations(report)

            expect(postMock).toHaveBeenCalledTimes(0);
        })
    })

    describe('When uploading the annotations to code-insights when there are under 100 vulnerabilities', () => {
        it('It should send one POST request to the correct endpoint.', async () => {
            const report = Report.createFromYarnAuditFile(`${__dirname}/../test-fixtures/yarn_audit.vulnerabilities.json`);
            await UploadAnnotations(report)

            expect(postMock).toHaveBeenCalledTimes(1);
        })
    })

    describe('When uploading the annotations to code-insights when there are over 100 vulnerabilities', () => {
        it('It should send multiple POST request to the correct endpoint.', async () => {
            const report = Report.createFromYarnAuditFile(`${__dirname}/../test-fixtures/yarn_audit.vulnerabilities.json`);

            while (report.getAnnotations().length <= 100) {
                report.addAnnotation({
                    annotation_type: "", details: "", external_id: "", link: "", severity: "", summary: ""
                });
            }

            await UploadAnnotations(report)

            expect(postMock).toHaveBeenCalledTimes(2);
        })
    })
})