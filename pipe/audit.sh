#!/bin/sh

DEBUG=${DEBUG:="false"}
PACKAGE_JSON_PATH=${PACKAGE_JSON_PATH:="."}
PACKAGE_MANAGER=${PACKAGE_MANAGER:="npm"}

write_debug() {
  if $DEBUG; then
    echo "[DEBUG] $1"
  fi
}

write_debug "Switching to clone directory '${BITBUCKET_CLONE_DIR}'."
cd "${BITBUCKET_CLONE_DIR}" || exit

write_debug "Package Json Path: $PACKAGE_JSON_PATH"

write_debug "Switching to package json directory '${PACKAGE_JSON_PATH}'."
cd ${PACKAGE_JSON_PATH} || exit

write_debug "Package Manager: $PACKAGE_MANAGER"
if [ "$PACKAGE_MANAGER" = "npm" ]; then
  if test -f "./package-lock.json"; then
    write_debug "Running npm audit to output json location."
    npm audit --json >> /pipe/audit_output.json
  else
    echo "[ERROR] package-lock.json not found."
    exit 1
  fi
elif [ "$PACKAGE_MANAGER" = "yarn" ]; then
  if test -f "./yarn.lock"; then
    write_debug "Running yarn audit to output json location."
    yarn audit --json >> /pipe/json-lines-ouput.json

    write_debug "Converting JSON lines format to normal json array."
    sed '1s/^/[/; $!s/$/,/; $s/$/]/' /pipe/json-lines-ouput.json > /pipe/audit_output.json
  else
    echo "[ERROR] yarn.lock not found."
  fi
else
  echo "[ERROR] File manager must be 'npm' or 'yarn'."
  exit 1
fi

write_debug "Running through nodejs report script."
node /pipe/dist/main.js /pipe/audit_output.json
